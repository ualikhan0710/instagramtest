//
//  CommentInputTextView.swift
//  Instagram
//
//  Created by Уали on 11/12/18.
//  Copyright © 2018 Уалихан. All rights reserved.
//

import UIKit

class CommentInputTextView:  UITextView {
    
    fileprivate let placeholderLabel : UILabel = {
        let label = UILabel()
        label.text = "Enter Comment"
        label.textColor = UIColor.lightGray
        return label
    }()
    
    func showPlaceholedLabel(){
        placeholderLabel.isHidden = false
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleTextChange), name: UITextView.textDidChangeNotification , object: nil)
        
        addSubview(placeholderLabel)
        placeholderLabel.anchor(top:  topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    //text view change 
    @objc func handleTextChange() {
        placeholderLabel.isHidden = !self.text.isEmpty
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
